package com.app.service;

import java.util.List;

import org.springframework.web.bind.annotation.RequestParam;

import com.app.entity.Transaction;
import com.app.entity.User;
import com.app.model.FundTransfer;
import com.app.model.RegisterDto;
import com.app.model.ResponseDto;
import com.app.service.impl.TransactionHistory;

public interface BankingApplicationService {
	
	public ResponseDto registerUser(RegisterDto registerDto);

	List<Transaction> transfer(FundTransfer fundTransfer);
	List<Transaction> getMonthlybankstatement(String month);
	
}
