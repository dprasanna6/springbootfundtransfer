package com.app.repository;

@Repository
public interface TransactionHistory extends JpaRepository<Transaction,Integer> {
	
	@Query("select t from Transaction t where t.date like %:month% ")
	List<Transaction> findRecordsByMonth(String month);

}
